import React, {Component} from 'react';
// import ReactDOM, {render} from "react-dom";
// import {BrowserRouter, Route, Link, NavLink} from 'react-router-dom';
import {BrowserRouter, Route, NavLink} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Section001 from "./forms/section001.js";
import Section002 from "./forms/section002.js";
import Section003 from "./forms/section003.js";

class FormNavigationDemo extends Component {
	render() {
		const template = (
			<div className="container">
				<div className="py-5">
					<BrowserRouter>
						<div className="row">
							<div className="col-md-3">
								<div className="list-group">
									<NavLink className="list-group-item list-group-item-action" activeClassName="active" to="/section001">User Details</NavLink>
									<NavLink className="list-group-item list-group-item-action" activeClassName="active" to="/section002">Address Details</NavLink>
									<NavLink className="list-group-item list-group-item-action" activeClassName="active" to="/section003">Education Detial</NavLink>
								</div>
							</div>
							<div className="col-md-9">
								<Route exact path="/" component={Section001} />
								<Route path="/section001" component={Section001} />
								<Route path="/section002" component={Section002} />
								<Route path="/section003" component={Section003} />
							</div>
						</div>
					</BrowserRouter>
				</div>
			</div>
		);
		return (template);
	}
}

export default FormNavigationDemo;