import React from 'react'
import {Link} from 'react-router-dom';
import validator from 'validator';
class Section001 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			errors:{}
		}
		this.validateForm = this.validateForm.bind(this);
	}
	validateForm(event) {
		let formData={}, errors = {}, hasErrors = false;
		for(var field in this.refs){
			formData[field] = this.refs[field].value;
		}
		console.log("Form Data");
		console.log(formData);

		if(!validator.isAlpha(formData.firstname)){
			hasErrors = true;
			errors.firstname = "Please enter valid name";
		}
		if(!validator.isAlpha(formData.lastname)){
			hasErrors = true;
			errors.lastname = "Please enter valid name";
		}
		if(!validator.isEmail(formData.email)){
			hasErrors = true;
			errors.email = "Please enter valid email";
		}
		
		this.setState({errors:errors});
		if(hasErrors){			
			event.preventDefault();
		}
	}
	render() {
		let {errors} = this.state;
		return (
			<div className="card">
				<div className="card-header">User Details</div>
				<div className="card-body">
					<form>
						<div className="form-group">
							<label htmlFor="firstname">First Name:</label>
							<input type="text" className="form-control" id="firstname" name="firstname" ref="firstname" placeholder="First Name" />
							<small className="form-text text-danger">{errors.firstname}</small>
						</div>
						<div className="form-group">
							<label htmlFor="lastname">Last Name:</label>
							<input type="text" className="form-control" id="lastname" name="lastname" ref="lastname" placeholder="Last Name" />
							<small className="form-text text-danger">{errors.lastname}</small>
						</div>
						<div className="form-group">
							<label htmlFor="email">Email Address:</label>
							<input type="email" className="form-control" id="email" name="email" ref="email" placeholder="Email" />
							<small className="form-text text-danger">{errors.email}</small>
						</div>
						<div className="form-group"> 
							<label  htmlFor="bday"> Birthday: </label>
							<input type="date" className="form-control" id="bday" name="bday" ref="bday" />
						</div>
						<div className="form-group">
							<label htmlFor="gender">Gender</label>
							<select className="form-control" id="gender" name="gender" ref="gender" >
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div> 
					</form>
				</div>
				<div className="card-footer">
					<Link to="/section002" onClick={this.validateForm} className="btn  btn-dark float-right">Next</Link>
				</div>
			</div>
		);
	}
}
export default Section001;