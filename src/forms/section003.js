import React from 'react'
import {Link} from 'react-router-dom';
import validator from 'validator';

class Section001 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			errors:{}
		}
		this.validateForm = this.validateForm.bind(this);
	}
	validateForm(event) {
		let formData={}, errors = {}, hasErrors = false;
		for(var field in this.refs){
			formData[field] = this.refs[field].value;
		}
		console.log("Form Data");
		console.log(formData);

		if(!validator.isAlpha(formData.degree)){
			hasErrors = true;
			errors.degree = "Please enter valid degree";
		}
		if(!validator.isNumeric(formData.year)){
			hasErrors = true;
			errors.year = "Please enter valid year";
		}
		if(validator.isEmpty(formData.university)){
			hasErrors = true;
			errors.university = "Please enter valid university";
		}
		if(!validator.isNumeric(formData.percentage)){
			hasErrors = true;
			errors.percentage = "Please enter valid percentage";
		}
		this.setState({errors:errors});
		if(hasErrors){			
			event.preventDefault();
		}
	}
	render() {
		let {errors} = this.state;
		return (
			<div className="card">
				<div className="card-header">Education Detail</div>
				<div className="card-body">
					<form>
						<div className="form-group">
							<label htmlFor="degree">Degree</label>
							<input type="text" className="form-control" id="degree" name="degree" ref="degree" placeholder="Degree" />
							<small className="form-text text-danger">{errors.degree}</small>
						</div>
						<div className="form-group">
							<label htmlFor="year">Year</label>
							<input type="text" className="form-control" id="year" name="year" ref="year" placeholder="Year" />
							<small className="form-text text-danger">{errors.year}</small>
						</div>
						<div className="form-group">
							<label htmlFor="university">Board/University</label>
							<input type="text" className="form-control" id="university" name="university" ref="university" placeholder="Board/University" />
							<small className="form-text text-danger">{errors.university}</small>
						</div>
						<div className="form-group">
							<label htmlFor="percentage">Percentage</label>
							<input type="text" className="form-control" id="percentage" name="percentage" ref="percentage" placeholder="Percentage" />
							<small className="form-text text-danger">{errors.percentage}</small>
						</div>
					</form>
				</div>
				<div className="card-footer text-muted">
					<Link to="/section002" className="btn  btn-outline-dark float-left">Previous</Link>
					<button className="btn btn-dark float-right" onClick={this.validateForm}>Save</button>
				</div>
			</div>
		);
	}
}
export default Section001;
