import React from 'react'
import {Link} from 'react-router-dom';
import validator from 'validator';

class Section001 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			errors:{}
		}
		this.validateForm = this.validateForm.bind(this);
	}
	validateForm(event) {
		let formData={}, errors = {}, hasErrors = false;
		for(var field in this.refs){
			formData[field] = this.refs[field].value;
		}
		console.log("Form Data");
		console.log(formData);

		if(validator.isEmpty(formData.address)){
			hasErrors = true;
			errors.address = "required";
		}
		if(validator.isEmpty(formData.city)){
			hasErrors = true;
			errors.city = "required";
		}
		if(validator.isEmpty(formData.state)){
			hasErrors = true;
			errors.state = "required";
		}
		if(!validator.isNumeric(formData.zipcode)){
			hasErrors = true;
			errors.zipcode = "Please enter valid zipcode";
		}
		
		this.setState({errors:errors});
		if(hasErrors){			
			event.preventDefault();
		}
	}
	render() {
		let {errors} = this.state;
		return (
			<div className="card">
				<div className="card-header">Address Detail</div>
				<div className="card-body">
					<form>
						<div className="form-group">
							<label htmlFor="address">Address</label>
							<textarea className="form-control" id="address" name="address" ref="address" placeholder="Address" rows="4" cols="50">
							</textarea>
							<small className="form-text text-danger">{errors.address}</small>
						</div>
						<div className="form-group">
							<label htmlFor="city">City</label>
							<input type="text" className="form-control" id="city" name="city" ref="city" placeholder="City" />
							<small className="form-text text-danger">{errors.city}</small>
						</div>
						<div className="form-group">
							<label htmlFor="state">State</label>
							<input type="text" className="form-control" id="state" name="state" ref="state" placeholder="State" />
							<small className="form-text text-danger">{errors.state}</small>
						</div>
						<div className="form-group">
							<label htmlFor="zipcode">ZIP Code</label>
							<input type="text" className="form-control" id="zipcode" ref="zipcode" name="zipcode" placeholder="ZIP Code" />
							<small className="form-text text-danger">{errors.zipcode}</small>
						</div>
					</form>
				</div>
				<div className="card-footer">
					<Link to="/section001" className="btn btn-outline-dark float-left">Previous</Link>
					<Link to="/section003" onClick={this.validateForm} className="btn  btn-dark float-right">Next</Link>
				</div>
			</div>
		);
	}
}
export default Section001;
