## Install Node.js if not already installed
[Follow the link for installation guide](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages)

## Navigate to the extracted directory 'reactdemo'
`cd reactdemo/`

## Install required node modules to start the project
`npm install`

## Start the server
`npm start`

Runs the app in the development mode.<br>
Open [localhost:3000](http://localhost:3000) to view it in the browser.